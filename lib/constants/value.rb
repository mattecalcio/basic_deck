module Value
  ACE = { name: 'ace', value: 1 }.freeze
  TWO = { name: 'two', value: 2 }.freeze
  THREE = { name: 'three', value: 3 }.freeze
  FOUR = { name: 'four', value: 4 }.freeze
  FIVE = { name: 'five', value: 5 }.freeze
  SIX = { name: 'six', value: 6 }.freeze
  SEVEN = { name: 'seven', value: 7 }.freeze
  EIGHT = { name: 'eight', value: 8 }.freeze
  NINE = { name: 'nine', value: 9 }.freeze
  TEN = { name: 'ten', value: 10 }.freeze
  JACK = { name: 'jack', value: 11 }.freeze
  QUEEN = { name: 'queen', value: 12 }.freeze
  KING = { name: 'king', value: 13 }.freeze
  ALL = [ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING].freeze
end
