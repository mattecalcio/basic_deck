module Color
  RED = 'red'.freeze
  BLACK = 'black'.freeze
  ALL = [RED, BLACK].freeze
end
