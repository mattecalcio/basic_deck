module Suit
  DIAMONDS = { name: 'diamonds', color: Color::RED }.freeze
  SPADES = { name: 'spades', color: Color::BLACK }.freeze
  HEARTS = { name: 'hearts', color: Color::RED }.freeze
  CLUBS = { name: 'clubs', color: Color::BLACK }.freeze
  ALL = [DIAMONDS, SPADES, HEARTS, CLUBS].freeze
end
