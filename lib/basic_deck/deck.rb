class BasicDeck::Deck
  attr_reader :cards

  def initialize
    # initialize deck of cards based on constants
    @cards = Suit::ALL.map do |suit|
      Value::ALL.map { |value| BasicDeck::Card.new(value, suit) }
    end
    @cards.flatten!
  end

  def shuffle
    if block_given?
      # custom shuffle
      yield @cards
    else
      # standard shuffle
      @cards.shuffle!
    end
  end
end
