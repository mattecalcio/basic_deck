class BasicDeck::Card

  def initialize(value, suit)
    set_value(value)
    set_suit(suit)
  end

  def name
    @value[:name]
  end

  def value
    @value[:value]
  end

  def suit
    @suit[:name]
  end

  def color
    @suit[:color]
  end

  class InvalidValue < StandardError; end
  class InvalidSuit < StandardError; end

  private

  def set_value(value)
    # validate value
    raise InvalidValue unless Value::ALL.include?(value)
    @value = value
  end

  def set_suit(suit)
    # validae suit
    raise InvalidSuit unless Suit::ALL.include?(suit)
    @suit = suit
  end
end

