require 'spec_helper'

RSpec.describe BasicDeck::Deck do
  context 'initialize deck' do
    it 'valid' do
      deck = BasicDeck::Deck.new
      expect(deck.cards.size).to eq(Suit::ALL.size * Value::ALL.size)
    end
  end

  context 'shuffle' do
    it 'uses standard' do
      deck = BasicDeck::Deck.new
      dup_cards = deck.cards.dup
      expect(deck.shuffle).not_to eq(dup_cards)
    end

    it 'uses custom' do
      deck = BasicDeck::Deck.new
      dup_cards = deck.cards.dup
      dup_cards.rotate!(5)
      expect(deck.shuffle { |cards| cards.rotate!(5) }).to eq(dup_cards)
    end
  end
end
