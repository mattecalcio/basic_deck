require 'spec_helper'

RSpec.describe BasicDeck::Card do
  context 'success' do
    it 'initialize with valid values' do
      card = BasicDeck::Card.new(Value::ACE, Suit::DIAMONDS)
        aggregate_failures do
          expect(card.value).to eq(Value::ACE[:value])
          expect(card.suit).to eq(Suit::DIAMONDS[:name])
          expect(card.color).to eq(Color::RED)
        end
    end
  end

  context 'error' do
    it 'invalid value' do
      expect { BasicDeck::Card.new({name: 'star', value: 20}, Suit::DIAMONDS) }.to raise_error(BasicDeck::Card::InvalidValue)
    end

    it 'invalid suit' do
      expect { BasicDeck::Card.new(Value::ACE, {name: 'star', value: 20, color: Color::RED}) }.to raise_error(BasicDeck::Card::InvalidSuit)
    end
  end
end
