
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "basic_deck/version"

Gem::Specification.new do |spec|
  spec.name          = "basic_deck"
  spec.version       = BasicDeck::VERSION
  spec.authors       = ["Matteo Bellistri"]
  spec.summary       = 'Library to represent a standard deck of cards'

  spec.files         = `git ls-files`.split("\n").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16.a"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
